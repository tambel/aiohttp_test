from aiohttp import web
import aioredis
import asyncio
from aiohttp_session import session_middleware
from aiohttp_session.redis_storage import RedisStorage
from aiohttp_cache import setup_cache, RedisConfig, cache_middleware, RedisCache
from views import init_templates, home, new_request, search, socket, history
from models import init_pg
from my_async_stackexchange_api import MyAsyncApi


async def create_data_base(app, recreate_db=True):
    app["config"] = {"user": 'laptop',
                     "database": 'mydb',
                     "host": '127.0.0.1',
                     "password": 'q'}
    await  init_pg(app, recreate_db)


# update saved stack exchange requests on background
async def update_db(app):
    try:
        while not app["closing"]:
            await asyncio.sleep(app["update_delay"])
            await MyAsyncApi.storage.update_all()
            for ws in app["websockets"]:
                ws.send_str("$UPDATED")
    except asyncio.CancelledError:
        pass


async def start_background_tasks(app, loop):
    app["closing"] = False
    app["update_delay"] = 600
    app['db_updater'] = loop.create_task(update_db(app))


async def make_app(loop):
    pool = await aioredis.create_pool(('localhost', 6379))
    session_middleware_storage = session_middleware(RedisStorage(pool, httponly=False))

    app = web.Application(middlewares=[session_middleware_storage])
    app['websockets'] = list()
    await start_background_tasks(app, loop)

    app.router.add_static('/static/',
                          path='static',
                          name='static')

    await create_data_base(app, recreate_db=False)
    app.router.add_get('/', home)
    app.router.add_get('/home', home)
    app.router.add_get('/request', new_request)
    app.router.add_get('/search', search)
    app.router.add_get('/socket', socket)
    app.router.add_get('/history', history)
    init_templates(app)
    MyAsyncApi.init(app["db"])
    return app


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(make_app(loop))
    redis_config = RedisConfig(db=4,
                               key_prefix="my_example")
    # cache refresh is 60 seconds
    redis_config.expiration = 60
    setup_cache(app, cache_type="redis", backend_config=redis_config)
    web.run_app(app, host='127.0.0.1', port=8080)
