import aiohttp_jinja2
import jinja2, datetime
from aiohttp_session import get_session
from aiohttp import web, WSMsgType
import aiohttp_cache
from my_async_stackexchange_api import MyAsyncApi


def send_base(f):
    async def wrapped(request):
        session = request.session
        r = await f(request)
        r.update({"session": session, "from_timestamp": datetime.datetime.utcfromtimestamp})
        return r

    return wrapped


@aiohttp_jinja2.template('home.html')
@send_base
@aiohttp_cache.cache()
async def home(request):
    items = await MyAsyncApi.storage.get_saved_requests()
    return {"items": items}


@aiohttp_jinja2.template('history.html')
@send_base
@aiohttp_cache.cache()
async def history(request):
    request_id = int(request.rel_url.query.get("id"))
    items = await MyAsyncApi.storage.get_request_questions(request_id)
    text = request.rel_url.query.get("text")
    page = int(request.rel_url.query.get("page", "1"))
    rev = int(request.rel_url.query.get("rev", 0))
    page_size = int(request.rel_url.query.get("page_size", "25"))
    sort = request.rel_url.query.get("sort", "last_activity_date")
    request.session["current_page"] = page
    request.session["text"] = text
    request.session["id"] = request_id
    request.session["reversed"] = int(not rev)

    def get_key(e):
        k = e.get(sort)

    if items:
        items = sorted(items, key=lambda k: k.get(sort) if k.get(sort) else 0)
        if rev:
            items = reversed(items)
    return {"items": items}


@aiohttp_jinja2.template('new_request.html')
@send_base
@aiohttp_cache.cache()
async def new_request(request):
    session = await get_session(request)
    # session["count"]+=1
    return {}


@aiohttp_jinja2.template('search.html')
@send_base
@aiohttp_cache.cache()
async def search(request):
    title = request.rel_url.query.get("title")
    page = int(request.rel_url.query.get("page"))
    page_size = int(request.rel_url.query.get("page_size", "25"))
    new = bool(int(request.rel_url.query.get("new")))
    sort = request.rel_url.query.get("sort", "activity")
    request_id = await MyAsyncApi.storage.new_request(title) if new else await MyAsyncApi.storage.get_request_id(title)
    data = await MyAsyncApi.search(title, page, sort, page_size=page_size)
    items = data.get("items", list())
    if items:
        if request_id:
            await MyAsyncApi.storage.write_page(request_id, items)
    request.session["block_back"] = False
    request.session["block_forward"] = False
    if page == 1:
        request.session["block_back"] = True
    if not items:
        request.session["block_forward"] = True
    request.session["current_page"] = page
    request.session["text"] = title
    return {"items": items}


def handle_client_command(command, session):
    command, data = command.split("$")
    session_unchanged = True
    if command == "page_size":
        session["page_size"] = int(data)
    else:
        session_unchanged = False
    return session_unchanged


async def socket(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    app = request.app
    app["websockets"].append(ws)
    async for msg in ws:
        if msg.type == WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                if handle_client_command(msg.data, request.session):
                    storage = request.get('aiohttp_session_storage')
                    await storage.save_session(request, ws, request.session)
        elif msg.type == WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())
    app["websockets"].remove(ws)
    return ws


async def session_processor(request):
    session = await get_session(request)
    setattr(request, "session", session)
    if session.get("page_size") is None:
        session["page_size"] = 25
    return {}


def init_templates(app):
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'),
                         context_processors=[session_processor, aiohttp_jinja2.request_processor])
