import json, datetime

import aiohttp, asyncio
from db import insert, get_last_index
from models import request_table, request_to_question_table, question_table
import sqlalchemy as sa


class MyAsyncApi(object):
    storage = None

    def __init__(self):
        pass

    @classmethod
    def init(cls, db):
        cls.storage = Storage(db)

    @staticmethod
    async def search(title, page, sort, page_size=25):
        url = "https://api.stackexchange.com/2.2/search/advanced?page={page}&pagesize={page_size}&order=desc&" \
              "sort={sort}&title={title}&site=stackoverflow".format(title=title, page=page, page_size=page_size,
                                                                    sort=sort)
        response = await aiohttp.request('get', url)
        data = json.loads((await response.read()).decode())
        return data


def acquired(f):
    async def wrapper(*args, **kwargs):
        if len(args) and hasattr(args[0], "db"):
            async with (args[0].db.acquire()) as conn:
                r = await f(*args, conn, **kwargs)
            return r

    return wrapper


def prepare_question_data(data):
    result = dict()
    data["title"] = data["title"].replace("%", "%%")
    for k, v in data.items():
        if k == "owner":
            uid = v.get("user_id")
            result[k] = uid if uid else -1
        elif k == "tags":
            result[k] = 2
        elif k == "migrated_from" or k == "migrated_to":
            result[k] = 3

        else:
            result[k] = v

    return result


class Storage(object):
    def __init__(self, db):
        self.db = db

    @acquired
    async def new_request(self, text, connection):
        r_id = await self.get_request_id(text)
        if r_id:
            await self.delete_request(r_id)
        return await insert(connection, request_table, return_current=True, text=text,
                            time=int(datetime.datetime.now().timestamp()))

    @acquired
    async def write_page(self, id, data, connection):
        if data:
            for question in data:
                question_data = prepare_question_data(question)
                # current_ques = await insert(connection, question_table, return_current=True, **question_data)
                sel = sa.select([question_table.c.id]).where(
                    question_table.c.question_id == question_data["question_id"])
                r = await connection.execute(sel)
                q_id = None
                if r.rowcount:
                    q_id = (await r.first()).as_tuple()[0]
                if q_id:
                    await connection.execute(
                        question_table.update().values(**question_data).where(question_table.c.id == q_id))
                else:
                    await connection.execute(question_table.insert().values(**question_data))
                    q_id = await get_last_index(connection, question_table)

                sel = sa.select([request_to_question_table.c.id]).where(
                    request_to_question_table.c.question_id == q_id and request_to_question_table.c.request_id == id)
                r = await connection.execute(sel)
                r_id = None
                if r.rowcount:
                    r_id = (await r.first()).as_tuple()[0]
                if r_id:
                    await connection.execute(
                        request_to_question_table.update().values(request_id=id, question_id=q_id).where(
                            request_to_question_table.c.id == r_id))
                else:
                    await connection.execute(request_to_question_table.insert().values(request_id=id, question_id=q_id))
        return

    @acquired
    async def read_page(self, title, number, connection):
        pass

    @acquired
    async def request_exist(self, title, connection):
        pass

    @acquired
    async def get_request_id(self, title, connection):
        s = await connection.execute(sa.select([request_table.c.id]).where(request_table.c.text == title))
        if s.rowcount:
            return (await s.first()).as_tuple()[0]

    @acquired
    async def delete_request(self, id, connection):

        await connection.execute(request_table.delete().where(request_table.c.id == id))
        relations = sa.select([request_to_question_table.c.question_id]).where(
            request_to_question_table.c.request_id == id)
        await  connection.execute(
            question_table.delete().where(question_table.c.id == relations.c.question_id))

        await  connection.execute(
            request_to_question_table.delete().where(request_to_question_table.c.request_id == id))

    @acquired
    async def get_saved_requests(self, connection):
        r = await  connection.execute(sa.select([request_table]))
        if r.rowcount:
            res = list()
            async for row in r:
                res.append({s: r for s, r in zip("id,text,time".split(","), row.as_tuple())})
            return res

    @acquired
    async def get_request_questions(self, id, connection):
        fstr = "title,last_activity_date,creation_date,score"
        s = sa.select([request_to_question_table.c.question_id]).where(
            request_to_question_table.c.request_id == id)
        s2 = sa.select([getattr(question_table.c, fs) for fs in fstr.split(",")]).where(
            question_table.c.id == sa.alias(s).c.question_id)
        r = await connection.execute(s2)
        if r.rowcount:
            res = list()
            async for row in r:
                res.append({s: r for s, r in zip(fstr.split(","), row.as_tuple())})
            return res

    async def update_all(self):
        pass


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(MyAsyncApi.search("python", 1))
    loop.close()
