from setuptools import setup

requires = [
    'aiohttp',
    'aiohttp_jinja2',
    'aiohttp_session[aioredis]',
    'sqlalchemy',
    'aiopg',
]

setup(
    name='stack_exchange_search',
    install_requires=requires,
)

