import psycopg2, sqlalchemy as sa

def stringify_params(data):
    keys = list()
    values = list()
    primary_key = rv = None
    for k, v in data.items():
        if k == 'primary_key':
            primary_key = v
            continue
        t = type(v)
        if t == str:
            rv = v
        elif t == bool:
            rv = int(v)
        else:
            rv = v
        keys.append(k)
        values.append("{quote}{value}{quote}".format(value=rv, quote="'" if type(rv) == str else "'"))
    return {"primary_key": primary_key, "keys": keys, "values": values}


def get_insert_cmd(table, data):
    return "INSERT INTO {table_name} ({keys}) VALUES ({values})".format(table_name=table.name,
                                                                        keys=",".join(data["keys"]),
                                                                        values=",".join(data["values"]))


async def upsert2(conn, table, primary_key="id", uniq_key=None, return_current=True, **kwargs):
    data = stringify_params(kwargs)
    ins_cmd = get_insert_cmd(table, data)
    upd_cmd = " ON CONFLICT ({primary_key}) DO UPDATE SET {values}".format(
        primary_key=uniq_key if uniq_key else primary_key,
        values=",".join(
            ["{}={}".format(k, v) for k, v in
             zip(data["keys"], data["values"])]))
    await conn.execute(ins_cmd + upd_cmd)
    if return_current:
        if uniq_key:
            r = await conn.execute(
                sa.select([getattr(table.c, primary_key)]).where(getattr(table.c, uniq_key) == kwargs[uniq_key]))
            if r.rowcount:
                return (await r.first()).as_tuple()[0]
        else:
            await get_last_index(conn, table, primary_key=primary_key)



async def get_last_index(conn, table, primary_key="id", **kwargs):
    cmd_str = "SELECT currval('{}_{}_seq')".format(table.name, primary_key)
    try:
        return (await (await conn.execute(cmd_str)).first()).as_tuple()[0]
    except psycopg2.OperationalError as e:
        return None


async def insert(conn, table, return_current=False, primary_key="id", **kwargs):
    data = stringify_params(kwargs)
    ins_cmd = get_insert_cmd(table, data)
    await conn.execute(ins_cmd)
    if return_current:
        cmd_str = "SELECT currval('{}_{}_seq')".format(table.name, primary_key)
        return (await (await conn.execute(cmd_str)).first()).as_tuple()[0]


async def select(conn, table, **kwargs):
    r = await conn.execute(table.select().where(table.c.text == "python"))


async def delete(conn, table, **kwargs):
    await conn.execute(table.delete().where(table.c.text == "python"))
