from aiopg.sa import create_engine
import sqlalchemy as sa
from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    Text,
    ForeignKey,
    Table,
)

metadata = sa.MetaData()

tbl = sa.Table('tbl', metadata,
               sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
               sa.Column('int2', sa.Integer),
               sa.Column('val', sa.String(255)))

request_table = Table('request', metadata,
                      Column('id', Integer, primary_key=True, autoincrement=True),
                      Column('text', Text, unique=True),
                      Column("time", Integer))

request_to_question_table = Table("request_to_question", metadata,
                                  Column('id', Integer, primary_key=True, autoincrement=True),
                                  Column('request_id', Integer, ForeignKey("request.id")),
                                  Column('question_id', Integer, ForeignKey("question.id")))

question_table = Table('question', metadata,
                       Column("id", Integer, primary_key=True, autoincrement=True),
                       Column("question_id", Integer, unique=True),
                       Column("title", Text),
                       Column("owner", Integer, ForeignKey("se_user.id")),
                       Column("is_answered", Boolean),
                       Column("view_count", Integer),
                       Column("answer_count", Integer),
                       Column("accepted_answer_id", Integer),
                       Column("score", Integer),
                       Column("last_activity_date", Integer),
                       Column("creation_date", Integer),
                       Column("last_edit_date", Integer),
                       Column("link", Text),
                       Column('tags', Integer),
                       Column("closed_date", Integer),
                       Column("closed_reason", Text),
                       Column("bounty_closes_date", Integer),
                       Column("bounty_amount", Integer),
                       Column("protected_date", Integer),
                       Column("locked_date", Integer),
                       Column("migrated_from", Integer),
                       Column("migrated_to", Integer),
                       Column("community_owned_date", Integer))


tag_table = Table("tag", metadata,
                  Column("id", Integer, primary_key=True),
                  Column("name", Text))

user_type_table = Table("se_user_type", metadata,
                        Column("id", Integer, primary_key=True),
                        Column("name", Text, unique=True))

user_table = Table("se_user", metadata,
                   Column("id", Integer, primary_key=True),
                   Column("reputation", Integer),
                   Column("type", Text),
                   Column("name", Text),
                   Column("link", Text))


def get_column(column):
    name = column.name

    str_col_type = str(column.type)
    primary = " PRIMARY KEY" if column.primary_key else ""
    pg_type = f_key = ""
    unique = "UNIQUE" if column.unique else ""
    if str_col_type == "INTEGER":
        if column.autoincrement is True:
            pg_type = "serial"
        else:
            pg_type = str_col_type.lower()
    elif str_col_type.startswith("VARCHAR"):
        length = column.type.length
        pg_type = "varchar({})".format(length)
    elif str_col_type == "TEXT":
        pg_type = str_col_type.lower()

    elif str_col_type == "BOOLEAN":
        pg_type = "boolean"
    else:
        pass

    if column.foreign_keys:
        keys = list()
        for fk in column.foreign_keys:
            fkc = str(fk.column)
            table, c = fkc.split(".")
            keys.append("references {}({})".format(table, c))
        f_key == "".join(keys)

    return "{name} {pg_type} {unique} {p_key}{f_key}".format(name=name, pg_type=pg_type, p_key=primary, f_key=f_key,
                                                             unique=unique)


for c in user_table.columns:
    get_column(c)


async def create_table(engine, table, recreate=True):
    async with engine.acquire() as conn:
        if recreate:
            await conn.execute('DROP TABLE IF EXISTS {}'.format(table.name))
        table_value_str = ", ".join([get_column(c) for c in table.columns])
        try:
            await conn.execute(
                '''CREATE TABLE {table_name} ({values})'''.format(table_name=table.name, values=table_value_str))
        except:
            pass


async def init_pg(app, recreate_db=True):
    conf = app['config']
    engine = await create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'])
    tables = [question_table, tag_table, user_type_table, user_table, request_table, request_to_question_table]
    if recreate_db:
        for t in tables:
            await create_table(engine, t)
    app['db'] = engine
